#!/usr/bin/env python

import csv
import sys
import os
import subprocess
import requests


def main():
    
    if len(sys.argv) < 2:
        print("Usage: {} CSV_FILE", sys.argv[0])
        exit(1)

    csv_file = sys.argv[1]

    # Make sure the provided file actually exists
    assert os.path.exists(csv_file)

    # Read csv into array.
    rows = list()
    with open(csv_file, "r") as f:
        csvreader = csv.reader(f)
        for row in csvreader:
            rows.append(row)

    for row in rows:
        email = row[0].strip()
        username = row[1].strip().lower()
        print("Creating account:")
        print("username: {}, Email: {}".format(username, email))
        password = generate_password()
        create_user(username, password)
        send_welcome(email, username, password)
        # invite_to_space(username)

def generate_password():
    r = subprocess.check_output(['xkcd-password', '-s-'], universal_newlines = False)
    r = r.decode()
    return r.replace('\n','')
    
def create_user(username, password):
    return subprocess.call(["./create_user.sh", "-u", username, "-p", password, "--no-admin"])

def send_welcome(email, username, password):
    cmd = "/home/synapse/synapse-stack/utils/local/send_registration_email.sh {} {} '{}'".format(email, username, password)
    ssh = ["ssh", "-i", os.path.expanduser("~/.ssh/soupandrifle_ed25519"), "root@soupandriflesco.org", cmd]
    return subprocess.run(ssh)

def invite_to_space(username, space_id=None):
    if space_id is None:
        # soupandriflesco
        space_id = "!DTxhYJQGOlzXFfMzHl:soupandriflesco.org"
    
    access_token = os.environ.get('MATRIX_ACCESS_TOKEN', None)
    if access_token is None:
        raise Exception("You must set MATRIX_ACCESS_TOKEN in your environment.")

    data = {
        "user_id": "@{}:soupandriflesco.org".format(username) 
    }

    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(access_token)
    }

    port = 8000
    # url = "http://localhost:{}/_synapse/admin/v1/join/{}".format(port, space_id)
    url = "http://localhost:8000/_synapse/admin/v1/server_version"
    
    # Forward port for local connection
    ssh = ["ssh", "-i", os.path.expanduser("~/.ssh/soupandrifle_ed25519"), "-f", "-q",
            "-L", "8008:localhost:{}".format(port), "root@soupandriflesco.org"]
    p = subprocess.Popen(ssh, shell=True)
    requests.post(url, headers=headers, data=data)

if __name__ == "__main__":
    main()
