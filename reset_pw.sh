#!/usr/bin/env bash
ssh -i ~/.ssh/soupandrifle_ed25519 root@soupandriflesco.org \
"/home/synapse/synapse-stack/utils/local/reset_password.sh $@"