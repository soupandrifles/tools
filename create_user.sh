#!/usr/bin/env bash
ssh -i ~/.ssh/soupandrifle_ed25519 root@soupandriflesco.org \
"cd /home/synapse/synapse-stack && docker-compose exec -T matrix register_new_matrix_user http://localhost:8008 -c /data/homeserver.yaml $@"

