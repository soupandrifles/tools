#!/usr/bin/env bash

email=$1
username=$2
password=$3
subject="Soup and Rifles - Matrix Account Details"

read -r -d '' message <<EOF
Welcome to Soup and Rifles Matrix Server.

The easiest way to get started is to point your browser to https://app.element.io/#/login
Under 'Homeserver', you should see 'matrix.org' with and Edit button to the right. 
Click Edit. Make sure 'Other Homeserver' is selected and enter 'soupandriflesco.org' and click 'Continue'
This should send you back to the login screen, and you should see 'soupandriflesco.org' where 'matrix.org' was before.
enter your username and the password as shown below. 

*NOTE* you should change your password as soon as possible. Click your user icon in the upper lefthand corner, select 'All settings'
Select 'General' and you can update it there. 

username: $username
password: $password  Seriously, change this ASAP.

- Pipes
EOF

echo "$message" | mail -s "$subject" $email 