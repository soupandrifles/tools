#!/usr/bin/env bash
set -e
usage() {
    echo "Usage: $0 @username:domain.name password"
    exit 1
}

unset -v user
unset -v password

user=$1
password=$2

if [ -z "$password" ] || [ -z "$user" ]; then
        echo 'Missing username or password' >&2
        usage
fi

IP=`docker container inspect synapse-stack_db_1 -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'`
cd /home/synapse/synapse-stack
source .env
export PGPASSWORD="$POSTGRES_PASSWORD"
echo "Reseting password to: $NEW_PASS for User: $user"
NEW_PASS=$password
NEW_PASS_HASH=`docker-compose exec -T matrix hash_password -p $NEW_PASS -c /data/homeserver.yaml`
SQL="UPDATE users set password_hash = '$NEW_PASS_HASH' where name = '$user';"
echo $SQL | psql -h $IP -U synapse -d synapse
